import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment';
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import vSelect from "vue-select";
// import Ads from 'vue-google-adsense'
import CoolLightBox from 'vue-cool-lightbox'
import 'vue-cool-lightbox/dist/vue-cool-lightbox.min.css'
import shareIt from 'vue-share-it';

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard);

Vue.use(shareIt);
Vue.use(require('vue-script2'));
Vue.use(CoolLightBox);

// Vue.use(Ads.Adsense);
// Vue.use(Ads.InArticleAdsense);
// Vue.use(Ads.InFeedAdsense);

Vue.component("v-select", vSelect);
import GoogleAuth from 'vue-google-oauth2'
const gauthOption = {
    clientId: '30173417980-1mr78rb4ietprqboovvm5j9hg3265fj2.apps.googleusercontent.com',
    scope: 'profile email',
    prompt: 'select_account',
};
Vue.use(GoogleAuth, gauthOption);

Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
});

import App from './view/App'
import Index from './pages/index'
import Artists from './pages/artists'
import About from './pages/about'
import Contact from './pages/contact'
import Login from './pages/login'
import Register from './pages/register'
import Profile_photo from './profile/photo'
import Track from './profile/track'
import Works from './pages/work'
import Donation from './profile/trans_logs'
import Subscriptions from './profile/sub_logs'
import Account from './profile/account'
import Withdrawals from './profile/withdrawal_logs'
import Edit from './profile/edit'
import Password from './profile/password'
import ArtistSearch from './pages/artist_search'
import Forget from './pages/forget'
import FAQ from './pages/faq'


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/faq',
            name: 'faq',
            component: FAQ,
        },
        {
            path: '/',
            name: 'index',
            component: Index,
        },
        {
            path: '/artists',
            name: 'artists',
            component: Artists,
        },
        {
            path: '/about',
            name: 'about',
            component: About,
        },
        {
            path: '/contact',
            name: 'contact',
            component: Contact,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/forget',
            name: 'forget',
            component: Forget,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },
        {
            path: '/profile/photo',
            name: 'profile_photo',
            component: Profile_photo,
        },
        {
            path: '/profile/track',
            name: 'profile_track',
            component: Track,
        },
        {
            path: '/artist/:id',
            name: 'artist_work',
            component: Works,
        },
        {
            path: '/logs/donations',
            name: 'donations',
            component: Donation,
        },
        {
            path: '/logs/subscriptions',
            name: 'subscription',
            component: Subscriptions,
        },
        {
            path: '/account',
            name: 'account',
            component: Account,
        },
        {
            path: '/logs/withdrawals',
            name: 'withdrawals',
            component: Withdrawals,
        },
        {
            path: '/profile/edit',
            name: 'profile_edit',
            component: Edit,
        },
        {
            path: '/profile/password',
            name: 'password',
            component: Password,
        },
        {
            path: '/artists/search/:id',
            name: 'artist_search',
            component: ArtistSearch,
        },

    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

