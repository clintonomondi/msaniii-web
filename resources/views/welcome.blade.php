<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Msaniii</title>
    <meta name="description" content="">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Powering artistry"/>
    <meta property="og:image" content="/images/tab.jpeg"/>
    <meta property="og:description" content="Msaniii is a platform that digitally links creators to their audiences."/>


    <!-- Favicons -->
    <link href="/images/tab.jpeg" rel="icon">
    <link href="/images/tab.jpeg" rel="apple-touch-icon">

    <!-- Google Fonts -->

    <!-- Vendor CSS Files -->
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="/assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/font-awesome.css">

    <script src="https://unpkg.com/vue-select@latest"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">
    <!-- Template Main CSS File -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/login.css" rel="stylesheet">
    <link href="/css/wizard.css" rel="stylesheet">
    <link href="/css/profile.css" rel="stylesheet">
    <link href="/css/work.css" rel="stylesheet">
    <link href="/css/sidebar.css" rel="stylesheet">
{{--    <link href="https://fonts.cdnfonts.com/css/gantic" rel="stylesheet">--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@400&display=swap" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-D1NCCEQ46R"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-D1NCCEQ46R');
    </script>


</head>

<body>
<div class="content2">
<div id="app">
    <app></app>
</div>
<script src="{{ mix('js/app.js') }}"></script>
</div>

<!-- ======= Footer ======= -->
<footer id="footer">


    <div class="container d-md-flex py-4">

        <div class="mr-md-auto text-center text-md-left">
            <div class="copyright">
                &copy; Copyright <strong><span>Msaniii</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flattern-multipurpose-bootstrap-template/ -->
               <a href="/Privacy policy draft.pdf" target="_blank">Privacy Policy</a>
            </div>
        </div>
        <div class="social-links text-center text-md-right pt-3 pt-md-0">
            <a href="https://twitter.com/MSANIII_" class="twitter"><i class="bx bxl-twitter"></i></a>
            <a href="https://www.facebook.com/profile.php?id=100070081083302" class="facebook"><i class="bx bxl-facebook"></i></a>
            <a href="https://instagram.com/msaniii_?utm_medium=copy_link" class="instagram"><i class="bx bxl-instagram"></i></a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"  ></script>--}}

<script src="/assets/vendor/jquery/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="/assets/vendor/php-email-form/validate.js"></script>
<script src="/assets/vendor/jquery-sticky/jquery.sticky.js"></script>
<script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="/assets/vendor/venobox/venobox.min.js"></script>
<script src="/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="/assets/vendor/aos/aos.js"></script>
<script src="/loader/center-loader.js"></script>
<!-- Template Main JS File -->
<script src="/assets/js/main.js"></script>
<script src="/js/wizard.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.min.js" integrity="sha512-RGbSeD/jDcZBWNsI1VCvdjcDULuSfWTtIva2ek5FtteXeSjLfXac4kqkDRHVGf1TwsXCAqPTF7/EYITD0/CTqw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.esm.js" integrity="sha512-rX92e1gJcy6G+ivRwDY5NnrDdGz37qBHqhNgDB9b9oT83N+vcKOs7GCcDTvKz/mFanYSTz+EoRi8SGlMOd3MoQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.js" integrity="sha512-LlFvdZpYhQdASf4aZfSpmyHD6+waYVfJRwfJrBgki7/Uh+TXMLFYcKMRim65+o3lFsfk20vrK9sJDute7BUAUw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/helpers.esm.js" integrity="sha512-334wuK4vkaONVysNjemyZ0HZDlNvZnjAEvrOu4LFn4fCrCfzWJjFG7wePPfZtW7bGmWvjN/r0RkwiaQ67+hsNg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/helpers.esm.min.js" integrity="sha512-5XGk12SGIo2btywdra6Pg6M+mHRnso/oZE4TVQ9+FAqHxG2dN8FHk4/fw+XXd9xUFSd0xatlMjEEBbwFgq7THw==" crossorigin="anonymous"></script>

</body>

</html>
